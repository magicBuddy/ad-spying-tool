import { Button, TextField } from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import React , { useEffect, useState } from "react";
import {languages} from '../Shared/languages';
import Axios from "axios";
import { devices_UAs } from "../Shared/devices_UAs";
import { Firebase } from "../Config/firebase.config";
import { useHistory } from "react-router";
import SnackbarToast from "../Utils/SnackbarToast";
import LoadingSpinner from "../Utils/LoadingSpinner";
import publicIp from "public-ip";

export default function Home() {

    const [language, setLanguage] = useState("");
    const [query, setQuery] = useState("");
    const [hl,setHl] = useState("");
    const [countryDomain, setCountryDomain] = useState("");
    const [searchURL, setSearchURL] = useState("");
    const [canonicalName, setCanonicalName] = useState("");
    const [geoTargets, setAllGeoTargets] = useState([]);
    const [device, setDevice] = useState("");
    const [userAgent, setUserAgent] = useState("");
    const [string, setString] = useState("");
    const [openSnackbar, setOpenSnackbar] = useState(false);
    const [messageForSnackbar, setMessageForSnackbar] = useState("");
    const [snackbarType, setSnackbarType] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const [loggedinUserEmail, setLoggedinUserEmail] = useState('');

    const history = useHistory();

    useEffect(() => {

        setIsLoading(true);
        Firebase.auth().onAuthStateChanged(user => {
            
            if (!user) history.push('/login');
            else {
                
                setLoggedinUserEmail(user.email);
                setIsLoading(false);
            }
        })

    }, []);
    
    useEffect(()=> {
    
        const geoTargetSelected = geoTargets.filter(geoTarget=> geoTarget["canonical_name"] === canonicalName)[0]
        
        if(language){
            const languageSelected = languages.filter(lang=> lang["language name"]===language)[0]
            const hlForLanguage = languageSelected["language code"]
            setHl(hlForLanguage)
        }

        if(geoTargetSelected){
            setCountryDomain(geoTargetSelected["domain"])
            setString(geoTargetSelected["string"])
        }

        if(device){
            const deviceSelected = devices_UAs.filter(device_UA => device_UA.Device === device)[0];
            setUserAgent(deviceSelected.UA);
        }

        if(!(!!language)){
            setHl("")
            setLanguage("")
        }

        if(!(!!geoTargetSelected)){
            setCanonicalName("")
            setCountryDomain("")
            setString("")
        }

        if(!(!!device)){
            setDevice("");
            setUserAgent("");
        }

    }, [canonicalName, language, device])

    async function searchResultHandler(e) {
        
        e.preventDefault();

        if(!query.trim().length){

            alert('Please enter query to proceed')
            return;

        } else {

            let ipAddress = "";

            try {

                ipAddress = await publicIp.v4({
                    fallbackUrls: [ 'https://ifconfig.co/ip' ]
                })

            } catch (err) {

                alert('Something went wrong, while fetching IP...')
                return;
            }

            const base64EncodeOfCanonicalName = btoa(canonicalName);
            const uule = `w+CAIQICI${string}${base64EncodeOfCanonicalName}`

            setSearchURL(`https://www.${countryDomain ? countryDomain : 'google.com/'}search?igu=1&q=${query}&hl=${hl}&uule=${uule}&adtest-useragent=${userAgent}&ip=${ipAddress}`);

            Axios.get(`https://us-central1-dhappa-j.cloudfunctions.net/search`, {
                params: {
                    countryDomain : countryDomain ? countryDomain : 'google.com/',
                    igu: 1,
                    q: query,
                    hl,
                    uule,
                    'adtest-useragent': userAgent,
                    ip: ipAddress,
                    user: loggedinUserEmail
                }
            })            
        }
    }

    const canonicalNameChangeHandler = (e)=>{

        Axios.get('https://us-central1-dhappa-j.cloudfunctions.net/searchGeoTargets', { params: {text: e.target.value}})
            
            .then(res => {

                if(res.data?.data?.status===1 && res.data?.data?.data.length>0) setAllGeoTargets(res.data?.data?.data)
                
                if(res.data?.data?.status===1 && !res.data?.data?.data.length) setAllGeoTargets([])
                
            })

            .catch(err => {

                alert('Oops..something went wrong!')
                return;
            })
    }

    const onLogoutUserClick = () => {

        setIsLoading(true);
        Firebase.auth().signOut().then(() => {
            
            // Sign-out successful.
            history.push('/login');

          }).catch((error) => {
            
            // An error happened.
            setOpenSnackbar(true);
            setSnackbarType("error");
            setMessageForSnackbar(error.message);     
            setIsLoading(false);
          });
          
    }

    const handleSnackbarClose = () => {

        setOpenSnackbar(false);
        setSnackbarType("");
        setMessageForSnackbar("");
     }
      
    return (
        
        <React.Fragment>

        <LoadingSpinner isLoading={isLoading} />

        <SnackbarToast
            handleSnackbarClose={handleSnackbarClose}
            openSnackbar={openSnackbar}
            snackbarType={snackbarType}
            message={messageForSnackbar}
        />       

        <div className="ad-spying-tool">
            <form onSubmit={searchResultHandler}>            
                <div className="row app-bar">
                    <div className="col-lg-2 col-md-2 mt-2 mb-2 canonical-name-list">
                        <Autocomplete
                        id="canonical-name-list"
                        options={geoTargets}
                        onChange={(e)=> {
                            if(!!e.target.innerText) {
                                setCanonicalName(e.target.innerText.split(" | ")[0])
                                document.getElementById('language-list').focus();                                
                            }
                            else {
                                setCanonicalName("")
                                setAllGeoTargets([])
                            }
                        }}
                        getOptionLabel={(geoTargets) => geoTargets["canonical_name"] +" | " +geoTargets["target_type"] + " | " +geoTargets["criteria_id"]}
                        renderInput={(params) => <TextField {...params} onChange={(e)=> canonicalNameChangeHandler(e)} label="Enter Location" variant="outlined" />}
                        />
                    </div>
                    <div className="col-lg-2 col-md-2 mt-2 mb-2 language-list">
                        <Autocomplete
                        id="language-list"
                        options={languages}
                        getOptionLabel={(languages) => languages["language name"]}
                        onChange={(e)=> {
                            if(!!e.target.innerText) {
                                setLanguage(e.target.innerText)
                                document.getElementById('search-field').focus();
                            }
                            else {
                                setLanguage("")
                                setHl("")
                            }
                        }}
                        renderInput={(params) => <TextField {...params} label="Language" variant="outlined" />}
                        />
                    </div>
                    {/* <div className="col-lg-2 col-md-4 mt-2 mb-2 device-list">
                        <Autocomplete
                        id="device-list"
                        options={devices_UAs}
                        onChange={(e)=> {
                            if(!!e.target.innerText) setDevice(e.target.innerText)
                            else {
                                setDevice("")
                                setUserAgent("")
                            }
                        }}
                        getOptionLabel={(devices_UAs) => devices_UAs["Device"]}
                        renderInput={(params) => <TextField {...params} label="Device" variant="outlined" />}
                        />
                    </div> */}
                    <div className="col-lg-4 col-md-4 mt-2 mb-2 search-field">
                        <TextField id = "search-field"
                        fullWidth
                        placeholder="What's on your mind?"
                        onChange={(e)=> setQuery(e.target.value)}
                        variant = "outlined"/>
                    </div>
                    <div className="col-lg-2 col-md-2 mt-2 mb-2 search-button text-left">
                        <Button variant = "contained"
                        color="primary"
                        type="submit"
                        style={{width: 120, height: 50}}
                        > Search
                        </Button>
                    </div>
                    <div className="col-lg-2 col-md-2 mt-2 mb-2 search-button text-right">
                        <Button variant = "contained"
                        color="primary"
                        type="button"
                        onClick={onLogoutUserClick}
                        style={{width: 120, height: 50}}
                        > Logout
                        </Button>
                    </div>
                </div>
            </form>

            <div className="search-result row">
                <iframe title="Google search results" style={{border: "none"}} id="search-result" width="100%"  src={searchURL.trim().length===0 ? `https://www.google.co.in/search?igu=1` : searchURL}>
                </iframe>
            </div>
        </div>
        </React.Fragment>
    );
}